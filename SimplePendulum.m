clc; clear all; close all;
%% Create the mupad notebook object
nb = mupad('SimplePendulum.mn');

%% Bring the notebook symbolic object into MATLAB
mySol = getVar(nb, 'sol');

%% Convert the symbolic object into a MATLAB function
myfunhandle = matlabFunction(mySol);

%% Convert the symbolic object into a MATLAB function file
myfunfile = matlabFunction(mySol,'file','myfunfile');

%% Convert the symbolic object into a MATLAB Simlulink Function block
new_system('mysys');
%
open_system('mysys');
%
matlabFunctionBlock('mysys/myfunblock',mySol);

